package com.itg.municipalityprocurementweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MunicipalityProcurementWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(MunicipalityProcurementWebApplication.class, args);
	}
}
